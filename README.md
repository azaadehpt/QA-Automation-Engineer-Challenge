## QA Automation Engineer Challenge Guidelines

### Project Scope

As a QA Engineer, create a e2e automation with [robotframework.org](robotframework.org) to demonstrate your test automation abilities.

## Goals

### Write a 1st end-to-end test
- Opens the landing page http://juliemr.github.io/protractor-demo/
- Verifies that the title is "Super Calculator"

### Write a 2nd end-to-end test
- Opens the landing page http://juliemr.github.io/protractor-demo/
- Verifies that 8+8=16

### Write a 3rd end-to-end test
- Opens the landing page http://juliemr.github.io/protractor-demo/
- Verifies that 16/4=4
- Verifies that 4*4=16
- Verifies that the History session has 2 expressions


## Evaluation and submission

Prioritized from most important to least important, here are our evaluation criteria:

- Acceptance Criteria: Have all acceptance criteria been fulfilled correctly?
- Code Quality: Is the code that you've written clean, well-structured and easy to understand?
- Documentation: Did you document how to run your tests well? Is your written communication clear and easy-to-understand?

We expect you to send us a link to your code on Gitlab or Github.

### Technology:
We recommend robotframework, but you can pick a different technology as well.

If you have any questions or feedback about the challenge, don't hesitate to reach out to us: tech-hiring@limehome.com
Good luck with the challenge! We are looking forward to your solution!
